'''Programa para pasar un numero 1 - 12 al mes correpondiente
entrada:
    num --> numero del mes  entre 1 y 12 
salida:
    imprime por pantalla el mes correspondiente al numero
Proceso:
    si num == 1
        imprime "Enero"
    sino si num == 2
        imprime "Febrero"
    sino si num == 3
        imprime "Marzo"
    sino si num == 4
        imprime "Abril"
    sino si num == 5
        imprime "Mayo"
    sino si num == 6
        imprime "Junio"
    sino si num == 7
        imprime "Julio"
    sino si num == 8
        imprime "Agosto"
    sino si num == 9
        imprime "Septiembre"
    sino si num == 10
        imprime "Octubre"
    sino si num == 11
        imprime "Noviembre"
    sino si num == 12
        imprime "Diciembre"
'''
num = int(input("Ingrese un numero 1-12 "))
if num == 1:
    print( "Enero")
elif num == 2:
    print( "Febrero")
elif num == 3:
    print( "Marzo")
elif num == 4:
    print( "Abril")
elif num == 5:
    print( "Mayo")
elif num == 6:
    print( "Junio")
elif num == 7:
    print( "Julio")
elif num == 8:
    print( "Agosto")
elif num == 9:
    print( "Septiembre")
elif num == 10:
    print( "Octubre")
elif num == 11:
    print( "Noviembre")
elif num == 12:
    print( "Diciembre")