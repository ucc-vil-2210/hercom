"""Este programa pasaa un numero a letras
datos de entrada:
    num es la variable de ingreso
datos de salida:
    imprime por pantalla el numero en letras
proceso:
    si num == 0 imprime "Cero"
    si num == 1 imprime "Uno"
    si num == 2 imprime "Dos"
    si num == 3 imprime "Tres"
    si num == 4 imprime "Cuatro"
    si num == 5 imprime "Cinco"
    si num == 6 imprime "Seis"
    si num == 7 imprime "Siete"
    si num == 8 imprime "Ocho"
    si num == 9 imprime "Nueve"
    si num !=  imprime "No es un numero valido"
"""
num = int(input("Ingrese un numero 0-9 "))
if num == 0:
    print("Cero")
elif num == 1:
    print("Uno")
elif num == 2:
    print("Dos")
elif num == 3:
    print("Tres")
elif num == 4:
    print("Cuatro")
elif num == 5:
    print("Cinco")
elif num == 6:
    print("Seis")
elif num == 7:
    print("Siete")
elif num == 8:
    print("Ocho")
elif num == 9:
    print("Nueve")
else:
    print("No es un numero valido")