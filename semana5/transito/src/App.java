/*Entrada:
    distancia: distancia está dada en metros
    tiempo: el tiempo estra dado en segundos
    velmax: la velocidad está dada en km/h
Salida:
    Si  tiempo>0
        si velmedia<velmax
            imprimir "OK"
        sino si velmedia < velmax+velmax*20%
            imprimir "Multa"
        sino si velmedia >= velmax+velmax*20%
            imprimir "Curso"
    sino
        imprimir "ERROR"
Proceso:
    distanciaKm = distancia/1000
    tiempoH =  tiempo / 3600
    velmedia = distanciaKm/tiempoH*/
public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        double distancia = 9165;
        double velmax = 110;
        double tiempo = 300;
        double distanciaKm = distancia/1000.0;
        double tiempoH =  tiempo / 3600.0;
        double velmedia = distanciaKm/tiempoH;
        if  (tiempo>0){
            if (velmedia<velmax){
                System.out.println("OK");
            }else{
                if (velmedia < velmax*1.2){
                    System.out.println("Multa");
                }else{
                    if (velmedia >= velmax*1.2){
                        System.out.println("Curso");
                    }
                }
            }
        }else{
            System.out.println("ERROR");
        }
    }
}
