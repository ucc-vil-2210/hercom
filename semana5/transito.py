'''
Entrada:
    distancia: distancia está dada en metros
    tiempo: el tiempo estra dado en segundos
    velmax: la velocidad está dada en km/h
Salida:
    Si  tiempo>0
        si velmedia<velmax
            imprimir "OK"
        sino si velmedia < velmax+velmax*20%
            imprimir "Multa"
        sino si velmedia >= velmax+velmax*20%
            imprimir "Curso"
    sino
        imprimir "ERROR"
Proceso:
    distanciaKm = distancia/1000
    tiempoH =  tiempo / 3600
    velmedia = distanciaKm/tiempoH
'''
distancia = int(input("Ingrese la distancia ")) 
tiempo = int(input("Ingrese el tiempo "))
velmax = int(input("Ingrese la velocidad Maxima "))
""" distancia = 9165 
tiempo = 300
velmax = 110 """
distanciaKm = distancia/1000
tiempoH =  tiempo / 3600
velmedia = distanciaKm/tiempoH
if  tiempo>0:
    if velmedia<velmax:
        print("OK")
    elif velmedia < velmax*1.2:
        print("Multa")
    elif velmedia >= velmax*1.2:
        print("Curso")
else:
    print("ERROR")