import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingrese una fecha para calcular el día después dd/mm/aaaa");
        String fecha = leer.nextLine();
        String[] datos = fecha.split("/");
        int dd = Integer.parseInt(datos[0]);
        int mm = Integer.parseInt(datos[1]);
        int aaaa = Integer.parseInt(datos[2]);
        if (dd>0 && dd<32 && mm>0 && mm<13){
            if (dd<28){
                dd ++;
            }else{
                if (mm==1||mm==3||mm==5||mm==7||mm==8||mm==10){
                    if (dd<31){
                        dd ++;
                    }else{
                        dd = 1;
                        mm ++;
                    }
                }else{
                    if (mm==12){
                        if (dd < 31){
                            dd ++;
                        }else{
                            dd = 1;
                            mm = 1;
                            aaaa ++;
                        }
                    }else{
                        if (mm==4||mm==6||mm==9||mm==11){
                            if (dd<30){
                                dd ++;
                            }else{
                                dd = 1;
                                mm ++;
                            }
                        }else{
                            if (aaaa % 4==0){
                                if (dd<29){
                                    dd ++;
                                }else{
                                    dd = 1;
                                    mm ++;
                                }
                            }else{
                                if (dd<28){
                                    dd ++;
                                }else{
                                    dd = 1;
                                    mm ++;
                                }
                            }
                        }
                    }
                }
            }
            System.out.println("La fecha del día despues de "+fecha+" es: "+dd+"/"+mm+"/"+aaaa);
        }else{
            System.out.println("La fecha "+fecha+" NO es valida");
        }
        
    }
}
