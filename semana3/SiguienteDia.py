'''Programa que calcula el siguiente día
Dada una fecha en formato dd/mm/aaaa, vamos a calcular cual es el 
siguiente día.

Entrada
    fecha cadena dd/mm/aaaa
Salida
    fecha cadena dd/mm/aaaa
procedimiento
    si dd<27
        SigDia=dd+1/mm/aaaa
    sino
        si mm esta en (1,3,5,7,8,10)
            si dd<31
                SigDia=dd+1/mm/aaaa
            sino
                SigDia=1/mm+1/aaaa
        sino si mm == 12
            si dd<31
                SigDia=dd+1/mm/aaaa
            sino
                SigDia=1/1/aaaa+1
        sino si mm esta en (4,6,9,11)
            si dd<30
                SigDia=dd+1/mm/aaaa
            sino
                SigDia=1/mm+1/aaaa
        sino si mm == 2
            si aaaa % 4 == 0
                si dd<29
                    SigDia=dd+1/mm/aaaa
                sino
                    SigDia=1/mm+1/aaaa  
            sino
                si dd<28
                    SigDia=dd+1/mm/aaaa
                sino
                    SigDia=1/mm+1/aaaa 
    imprimir SigDia
'''
fecha =  input("Ingrese una fecha dd/mm/aaaa ")
numeros = fecha.split('/')
dd = int(numeros[0])
mm = int(numeros[1])
aaaa = int(numeros[2])
print(dd)
print(mm)
print(aaaa)
bisiesto = (aaaa % 4 == 0)
if (dd>0 and dd<32 ) and (mm>0 and mm<13): #dd>=1 and dd<=31
    if dd < 28:
        dd += 1 # dd = dd + 1
    else:
        if mm in (1,3,5,7,8,10): # meses de 31 dias y no cambia el año
            if dd < 31: # dia del mes es menor a 31
                dd = dd + 1 # dd += 1
            else: # dia sera el 1 del siguiente mes
                dd = 1 
                mm += 1
        elif mm == 12: #Diciembre cambia el mes, el dia y el año suma 1 si es 31/12/aaaaa
            if dd < 31: # dia del mes es menor a 31
                dd = dd + 1 # dd += 1
            else:
                dd = 1
                mm = 1
                aaaa += 1
        elif mm in (4,6,9,11): # Meses de 30 dias
            if dd < 30:
                dd += 1
            else: # dia sera el 1 del siguiente mes
                dd = 1 
                mm += 1
        else:
            if aaaa % 4 == 0:
                if dd < 29: # dia del mes es menor a 31
                    dd = dd + 1 # dd += 1
                else: # dia sera el 1 del siguiente mes
                    dd = 1 
                    mm += 1
            else:
                if dd < 28: # dia del mes es menor a 31
                    dd = dd + 1 # dd += 1
                else: # dia sera el 1 del siguiente mes
                    dd = 1 
                    mm += 1
    print('El siguiente dia de ',fecha,' es ',dd,'/',mm,'/',aaaa)
else:        
    print("Esa fecha no existe ",fecha)