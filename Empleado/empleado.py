'''Un empleado desea conocer a cuánto dinero equivalen las retenciones 
exigidas por la ley en relación con los pagos que la compañía para la 
que trabaja le realiza mensualmente. Se ha firmado un contrato que le 
permite trabajar tres semanas al mes y 50 horas semanales. Con el 
propósito de verificar el valor total de los descuentos decide 
construir un programa en Python que le permita verificar el valor de 
su salario antes y después de realizar los descuentos. Después de 
consultar sobre la normatividad Colombiana y revisar con detalle su 
contrato laboral nota que debe tener en cuenta los siguientes 
aspectos: 
 
El valor de una hora de trabajo normal se obtiene dividiendo el 
salario base sobre 180. 

Las horas extras se liquidan con un recargo del 45% sobre el valor 
de una hora normal. 

Debido a buen desempeño de un empleado la empresa ocasionalmente 
otorga bonificaciones de 1.5% del salario base. 

El salario total antes de descuentos se calcula como la suma del 
salario base, más el valor de las horas extras, más las bonificaciones
(si las hay). 

Se descontará 5.5% del salario total antes de descuentos para el plan 
obligatorio de salud. 

Se descontará 5% del salario total antes de descuentos para el aporte 
a pensión. 

Se descontará 5% del salario total antes de descuentos para caja de 
compensación. 

 
Luego de considerar toda esta información, Camilo decide construir 
un programa que permita a cualquier empleado de la empresa verificar 
si los pagos son correctos. 
 
Entradas: 
    SalarioBase: salario del trabajador
    HorasExtras: cantidad de horas extras realizadas por el trabajador
    Bonificacion: 1 tiene bonificacion, 0 no tiene bonificacion

Salidas:
    SalarioSinDescuentos: el salario con las bonificaciones y las 
                         horas extras.
    SalarioConDescuentos: el SalarioSinDescuentos menos los descuentos 
                         de Salud, Pension y CC.

Problema:
    ValorHora = SalarioBase/180
    ValorHoraExtra = ValorHora * 1.45 o ValorHora + (ValorHora * 0.45)
    Si Bonificacion == 1 entonces
        ValorBonificacion = SalarioBase * 1.015
    Sino 
        ValorBonificacion = 0
    SalSinDescuento = SalarioBase + HorasExtras*ValorHoraExtra + ValorBonificacion
    Salud = SalSinDescuento * 0.055
    Pension = SalSinDescuento * 0.05
    CC = SalSinDescuento * 0.05
    SalConDescuento = SalSinDescuento - (Salud + Pension + CC)

Prueba de escritorio
    Entradas (1000000 0 0) 
    Salidas  (845000.0 1000000.0) 
    
    SalarioBase = 1000000
    HorasExtras = 0
    Bonificacion = 0

    ValorHora = 1000000/180 = 5555.556
    ValorHoraExtra = 5555.556 * 1.45 o 5555.556 + (5555.556 * 0.45) = 8055.5556
    Si Bonificacion == 1 entonces
        ValorBonificacion = SalarioBase * 0.015
    Sino 
        ValorBonificacion = 0
    SalSinDescuento = 1000000 + 0*8055.5556 + 0 = 1000000
    Salud = 1000000 * 0.055 = 55000
    Pension = 1000000 * 0.05 = 50000
    CC = 1000000 * 0.05 = 50000
    SalConDescuento = 1000000 - (55000 + 50000 + 50000) = 1000000 - 155000 = 845000

Entradas (2355255 2 1) 
Salidas (2052107.5 2428529.6) 

    ValorHora = 2355255/180 = 13084.75
    ValorHoraExtra = 13084.75 * 1.45 = 18972.8875
    Si Bonificacion == 1 entonces
        ValorBonificacion = 2355255 * .015 = 35328.825
    Sino 
        ValorBonificacion = 0
    SalSinDescuento = 2355255 + 2*18972.8875 + 35328.825 = 2428529.6
    Salud = 2428529.6 * 0.055 = 133569.128
    Pension = 2428529.6 * 0.05 = 121426.48
    CC = 2428529.6 * 0.05 = 121426.48
    SalConDescuento = 2428529.6 - (133569.128 + 121426.48 + 121426.48) = 2052107.5
'''
SalarioBase=2355255
HorasExtras = 2
Bonificacion = 1
ValorHora = SalarioBase/180
ValorHoraExtra = ValorHora + (ValorHora * 0.45)
if Bonificacion == 1:
    ValorBonificacion = SalarioBase * 0.015
else: 
    ValorBonificacion = 0
SalSinDescuento = SalarioBase + HorasExtras*ValorHoraExtra + ValorBonificacion
Salud = SalSinDescuento * 0.055
Pension = SalSinDescuento * 0.05
CC = SalSinDescuento * 0.05
SalConDescuento = SalSinDescuento - (Salud + Pension + CC)
print(SalConDescuento," - ",SalSinDescuento)