'''
Entrada: X un numero entero
Salida: conejos valor de fibonacci para el mes X
Proceso: 
    Mes2 = 0
    Mes1 = 0
    conejos = 1
    Mes = 1
    Mientras Mes < X entonces
        Mes += 1
        Mes2 = Mes1
        Mes1 = conejos
        conejos = Mes1 + Mes2
    imprimir conejos
'''
x = int(input("Cuantos meses quiere simular? "))
Mes2 = 0
Mes1 = 0
conejos = 1
Mes = 1
while Mes < x :
    Mes += 1
    Mes2 = Mes1
    Mes1 = conejos
    conejos += Mes2
print(conejos)

conejos = 1
Mes2 = 0
Mes1 = 0
for Mes in range(x-1):
    Mes2 = Mes1
    Mes1 = conejos
    conejos += Mes2
print(conejos)